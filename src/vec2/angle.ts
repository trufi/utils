export function vec2angle(v: number[]): number {
    return Math.atan2(v[1], v[0]);
}
